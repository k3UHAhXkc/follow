# Change Log
All notable changes to this project will be documented in this file.

This is the changelog for [follow](https://gitlab.com/k3UHAhXkc/follow)

## [0.1.5] - 2018-02-16
### Fixed
* Fix bug with file name display with names longer than the screen width.

## [0.1.4] - 2018-01-30
### Fixed
* workaround bad terminal size detection by using default of 80 cols when detected width is outside a range of values.
* upgrade some dependencies

## [0.1.2] - 2017-10-06
### Fixed
* upgrade dependencies

## [0.1.1] - 2017-02-07
### Fixed
* fix bug where a read one byte larger than the buffer causes a crash
* upgrade crates and rustfmt code

## [0.1.0] - 2017-01-30
### Changed
* initial release
