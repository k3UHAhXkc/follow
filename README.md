
        _|_|            _|  _|
      _|        _|_|    _|  _|    _|_|    _|          _|
    _|_|_|_|  _|    _|  _|  _|  _|    _|  _|    _|    _|
      _|      _|    _|  _|  _|  _|    _|    _|  _|  _|
      _|        _|_|    _|  _|    _|_|        _|  _|


# What is this thing

This is a program that watches files and prints to stdout what is being
written to them. In this way it is similar to `tail -F`. Planned are
some unique features however:

* Can follow all files matching a glob pattern. Newly
  created files that match the pattern will be detected and followed.
* Can filter output in various ways
* filter selection and config is interactive

# Why the glob thing?

This is useful for monitoring log files from a program that writes to
log files with timestamps in the filename, so it is constantly changing
which file it writes to. In this scenario its a pain to monitor the
application with `tail` or similar programs because the files they read
will eventually stop being written to.

# Example

    $ ls
    application.2016-12-01.08.log
    $
    #etc...
