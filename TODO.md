# cli

* [x] v1 setup docopt
* [x] v1 allow passing in a file path
* [x] allow passing in multiple file paths
* [x] allow passing in a glob
* [x] allow passing in multiple globs
* [ ] allow passing in a filter
* [ ] allow passing in multiple filters
* [ ] allow passing in config file with specifies globs
* [ ] allow passing in config file with specifies filters

# errors

* [x] v1 setup error chain

# file groups

* [ ] v1 store glob in group

# notify

* [ ] setup notify
* [ ] map globs to paths and register with notify
* [ ] read from channel
* [ ] on boot, create symlink if it doesn't yet exist
* [ ] when new file exists that matches a glob, update symlink

# alternative to notify

* [x] every N seconds, re-query which files match glob and maybe
  read from new files

# tokio

* [ ] v1 open stream to file
* [ ] open multiple streams to files

# filtering

* [ ] match output against active filter

# interactivity

* [ ] decide whether input will be on stdin or through signals or what
* [ ] research whether ncurses can play nicely with terminal scrolling,
  or if its always cvvis or whatever.
* [ ] build menu
* [ ] allow selecting within menu
* [ ] allow adding a new filter
* [ ] allow removing a filter

# logging

* [ ] write to log file in tests
* [ ] only write to log when run from command line when asked
