use docopt::{Docopt, Error};

const USAGE: &'static str = "
follow

Usage:
  follow [--verbose] <path>...
  follow (-h | --help)
  follow --version

Options:
  -h --help     Show this screen.
  --version     Show version.
  -v --verbose  Be verbose.
";

#[derive(Debug, RustcDecodable)]
pub struct Args {
    arg_path: Vec<String>,
    flag_verbose: bool,
}

impl Args {
    pub fn parse() -> Result<Args, Error> {
        Docopt::new(USAGE).and_then(|a| a.decode())
    }

    pub fn paths(&self) -> &Vec<String> {
        &self.arg_path
    }

    pub fn is_verbose(&self) -> bool {
        self.flag_verbose
    }
}
