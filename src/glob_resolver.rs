use errors::*;
use glob::MatchOptions;
use glob::glob_with;
use msg::Msg;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::thread;
use std::time::Duration;

pub struct GlobResolver {
    globs: Vec<String>,
    tx: Sender<Msg>,
    rx: Receiver<Option<()>>,
}

impl GlobResolver {
    pub fn spawn(globs: Vec<String>,
                 main_tx: Sender<Msg>,
                 self_rx: Receiver<Option<()>>)
                 -> Result<thread::JoinHandle<Result<()>>> {
        Ok(thread::spawn(move || GlobResolver::new(globs, main_tx, self_rx)?.run()))
    }

    fn new(globs: Vec<String>, tx: Sender<Msg>, rx: Receiver<Option<()>>) -> Result<GlobResolver> {
        Ok(GlobResolver {
            globs: globs,
            tx: tx,
            rx: rx,
        })
    }

    fn run(&mut self) -> Result<()> {
        let opts = MatchOptions::new();

        trace!("run starting");
        loop {
            trace!("starting loop");

            // check for exit signal
            match self.rx.try_recv() {
                Ok(_) => break,
                _ => {}
            }

            let mut paths = vec![];

            for pattern in &self.globs {
                match glob_with(pattern, &opts) {
                    Ok(entries) => {
                        for entry in entries {
                            match entry {
                                Ok(path) => {
                                    paths.push(path);
                                }
                                Err(_) => {}
                            }
                        }
                    }
                    Err(_) => {}
                }
            }

            trace!("found paths: {:?}", &paths);

            if paths.len() > 0 {
                self.tx.send(Msg::Paths(paths)).chain_err(|| "failed to send paths to main thr")?;
            }

            trace!("about to sleep");
            thread::sleep(Duration::from_secs(5));
        }
        trace!("run ending");

        Ok(())
    }
}
