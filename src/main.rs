#![recursion_limit = "1024"]

extern crate ansi_term;
extern crate chrono;
extern crate docopt;
#[macro_use]
extern crate error_chain;
extern crate fern;
extern crate glob;
#[macro_use]
extern crate log;
extern crate rustc_serialize;
extern crate terminal_size;

mod errors {
    // Create the Error, ErrorKind, ResultExt, and Result types
    error_chain!{}
}

mod args;
mod glob_resolver;
mod msg;
mod worker;

use ansi_term::Style;
use args::Args;
use errors::*;
use glob_resolver::GlobResolver;
use msg::Msg;
use std::borrow::Cow;
use std::collections::HashMap;
use std::collections::hash_map::Entry;
use std::io::prelude::*;
use std::path::PathBuf;
use std::sync::mpsc::Sender;
use std::sync::mpsc::channel;
use std::thread;
use terminal_size::{Width, terminal_size};
use worker::Buffer;
use worker::Worker;

fn main() {
    let args = Args::parse().unwrap_or_else(|e| e.exit());

    if let Err(ref e) = run(args) {
        println!("error: {}", e);

        for e in e.iter().skip(1) {
            println!("caused by: {}", e);
        }

        if let Some(backtrace) = e.backtrace() {
            println!("backtrace: {:?}", backtrace);
        }

        ::std::process::exit(1);
    }
}

fn run(args: Args) -> Result<()> {
    // setup_logging("log/follow.log")?;

    let (main_tx, main_rx) = channel();
    let mut workers: HashMap<PathBuf, (thread::JoinHandle<_>, Sender<_>)> = HashMap::new();
    let mut last_path_printed: Option<PathBuf> = None;

    let glob_resolver = {
        let (tx, rx) = channel();
        let handle = GlobResolver::spawn(args.paths().clone(), main_tx.clone(), rx)?;
        (tx, handle)
    };

    for msg in main_rx {
        match msg {
            Msg::Exit(path) => {
                trace!("received exit for path: {:?}", &path);
                print_msg(format!("{} went away", path.to_string_lossy()))?;
                if last_path_printed.as_ref() == Some(&path) {
                    last_path_printed = None;
                }

                match workers.remove(&path) {
                    Some((handle, _)) => {
                        handle.join().expect("couldn't join on thread that said it exited")?;
                    }
                    None => {
                        warn!("received exit method with unknown path: {:?}", &path);
                    }
                }
            }
            Msg::Paths(paths) => {
                trace!("new paths: {:?}, known paths: {:?}",
                       paths,
                       workers.keys().map(|k| k.to_string_lossy()).collect::<Vec<Cow<str>>>());
                for path in paths {
                    match workers.entry(path.clone()) {
                        Entry::Vacant(v) => {
                            trace!("watching path: {:?}", v.key());
                            match watch_file(path.clone(), main_tx.clone()) {
                                Ok(tuple) => {
                                    v.insert(tuple);
                                }
                                Err(e) => {
                                    error!("Tried to start watching path but failed. path: {:?}. \
                                            error: {:?}",
                                           v.key(),
                                           e);
                                }
                            }
                        }
                        Entry::Occupied(_) => {}
                    }
                }
            }
            Msg::Read(buf) => {
                if buf.get_bytes().len() > 0 {
                    if last_path_printed.as_ref() != Some(&buf.path) {
                        print_msg(buf.path.to_string_lossy())?;
                        last_path_printed = Some(buf.path.clone());
                    }
                    std::io::stdout().write(buf.get_bytes())
                        .chain_err(|| "failed to write to stdout")?;
                }
                let worker_tx = &workers.get(&buf.path).expect("unknown path??").1;
                worker_tx.send(Some(buf)).chain_err(|| "failed to send to worker")?;
            }
        };
    }

    for (_, (handle, _)) in workers {
        handle.join().expect("failed to join handle. TODO: add this to chain err somehow")?;
    }

    glob_resolver.0.send(None).chain_err(|| "failed to send exit msg to glob resolver")?;
    glob_resolver.1.join().expect("failed to join handle. TODO: add this to chain err")?;

    Ok(())
}

// Using unicode chars from here: https://unicode-table.com/en/blocks/box-drawing/
fn print_msg<S: std::fmt::Display>(msg: S) -> Result<()> {
    let width = match terminal_size() {
        Some((Width(w @ 20 ... 200), _)) => {
            trace!("detected terminal width: {}", &w);
            w as usize
        },
        Some(_) => 80,
        None => 80,
    };

    let msg = format!("{}", msg);
    let delta = if msg.len() <= width - 3 { width - msg.len() - 3 } else { 0 };
    let style = Style::new().fg(ansi_term::Colour::Blue).dimmed();

    if delta > 0 {
        let banner: String = (0..delta).map(|_| "━").collect();
        let msg = format!("\n{} {} {}\n", style.paint("╸"), msg, style.paint(banner));
        std::io::stdout().write(msg.as_bytes()).chain_err(|| "failed to write to stdout")?;
    } else {
        let banner: String = (0..width).map(|_| "━").collect();
        let msg = format!("\n{}\n{}\n", msg, style.paint(banner));
        std::io::stdout().write(msg.as_bytes()).chain_err(|| "failed to write to stdout")?;
    }

    Ok(())
}

fn watch_file(path: PathBuf,
              main_tx: Sender<Msg>)
              -> Result<(thread::JoinHandle<Result<()>>, Sender<Option<Buffer>>)> {
    let (worker_tx, worker_rx) = channel();
    let handle = Worker::spawn(path, main_tx, worker_tx.clone(), worker_rx)?;
    Ok((handle, worker_tx))
}

pub fn setup_logging(path: &str) -> Result<()> {
    let formatter = Box::new(|msg: &str, level: &log::LogLevel, location: &log::LogLocation| {
        format!("[{} {}] [{}] {}",
                chrono::Local::now().format("%Y-%m-%d %H:%M:%S%.3f"),
                level,
                location.module_path(),
                msg)
    });

    let config = fern::DispatchConfig {
        format: formatter,
        output: vec![fern::OutputConfig::file(path)],
        level: log::LogLevelFilter::Trace,
    };

    fern::init_global_logger(config, log::LogLevelFilter::Trace)
        .chain_err(|| "failed to init logger")?;

    Ok(())
}
