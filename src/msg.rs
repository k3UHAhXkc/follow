use worker::Buffer;
use std::path::PathBuf;

#[derive(Debug)]
pub enum Msg {
    Exit(PathBuf),
    Paths(Vec<PathBuf>),
    Read(Buffer),
}
