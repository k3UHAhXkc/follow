use errors::*;
use msg::Msg;
use std::borrow::Cow;
use std::fmt;
use std::fs::File;
use std::fs;
use std::io::BufReader;
use std::io::SeekFrom;
use std::io::prelude::*;
use std::ops::Range;
use std::path::PathBuf;
use std::sync::mpsc::Receiver;
use std::sync::mpsc::Sender;
use std::thread;
use std::time::Duration;

const BUFFER_SIZE: usize = 256;

pub struct Buffer {
    bytes: [u8; BUFFER_SIZE],
    pub path: PathBuf,

    // The size of last read, which is located in bytes at (0..size).
    size: usize,
}

impl Buffer {
    pub fn get_bytes(&self) -> &[u8] {
        &self.bytes[0..self.size]
    }
}

impl fmt::Debug for Buffer {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Buffer {{ path: {:?}, bytes: [_]}}", self.path)
    }
}

pub struct Worker {
    path: PathBuf,
    tx: Sender<Msg>,
    rx: Receiver<Option<Buffer>>,
    cursor: Option<u64>,
}

impl Worker {
    pub fn spawn(path: PathBuf,
                 main_tx: Sender<Msg>,
                 self_tx: Sender<Option<Buffer>>,
                 self_rx: Receiver<Option<Buffer>>)
                 -> Result<thread::JoinHandle<Result<()>>> {
        // double buffering
        for _ in 0..2 {
            let buf = Buffer {
                bytes: [0; BUFFER_SIZE],
                path: path.clone(),
                size: 0,
            };
            self_tx.send(Some(buf)).chain_err(|| "failed to bootstrap send buffer")?;
        }

        let handle = thread::spawn(move || Worker::new(path, main_tx, self_rx)?.run());

        Ok(handle)
    }

    fn new(path: PathBuf, tx: Sender<Msg>, rx: Receiver<Option<Buffer>>) -> Result<Worker> {
        Worker::new_with_cursor(path, tx, rx, None)
    }

    fn new_with_cursor(path: PathBuf,
                       tx: Sender<Msg>,
                       rx: Receiver<Option<Buffer>>,
                       cursor: Option<u64>)
                       -> Result<Worker> {
        Ok(Worker {
            path: path,
            tx: tx,
            rx: rx,
            cursor: cursor,
        })
    }

    fn run(&mut self) -> Result<()> {
        trace!("run starting");
        let mut file = {
            let file = match File::open(&self.path) {
                Ok(f) => f,
                Err(e) => {
                    trace!("file unable to open: {:?}", &self.path);
                    self.tx.send(Msg::Exit(self.path.clone())).chain_err(|| "unable to send")?;
                    return Err(e).chain_err(|| format!("failed to open file: {:?}", &self.path));
                }
            };
            BufReader::new(file)
        };
        trace!("file opened, waiting for buffer msg");

        for msg in &self.rx {
            trace!("received buffer msg");

            match msg {
                Some(mut buf) => {
                    trace!("msg is a buffer");

                    match self.read(&mut buf, &mut file) {
                        Ok(cursor) => {
                            self.cursor = Some(cursor);
                            self.tx.send(Msg::Read(buf)).chain_err(|| "foo")?;
                        }
                        Err(_) => {
                            trace!("file unable to read, breaking loop: {:?}", &self.path);
                            self.tx
                                .send(Msg::Exit(self.path.clone()))
                                .chain_err(|| "unable to send")?;
                            break;
                        }
                    }
                }
                None => {
                    trace!("msg is None, breaking");
                    break;
                }
            };
        }

        trace!("finished loop");

        Ok(())
    }

    // Read the given file into the buffer, and return the new cursor postion.
    //
    // Data is passed and returned just to get around borrowck issue where read can't take a `&mut
    // self` because it's called within a immutable borrow of self.rx.
    fn read(&self, buf: &mut Buffer, file: &mut BufReader<File>) -> Result<u64> {
        let available_range = self.poll()?;
        trace!("{}: available_range {:?}",
               self.path_as_lossy_str(),
               available_range);
        file.seek(SeekFrom::Start(available_range.start))
            .chain_err(|| "failed to seek")?;
        let available_size = (available_range.end - available_range.start) as usize;

        let count = {
            let mut slice = if available_size > buf.bytes.len() {
                &mut buf.bytes[(..)]
            } else {
                &mut buf.bytes[(0..available_size)]
            };

            file.read(&mut slice).chain_err(|| "error reading")?
        };
        trace!("{}: read count {:?}", self.path_as_lossy_str(), count);
        buf.size = count;
        Ok(available_range.start + count as u64)
    }

    // Polls file checking its file size against its known size. Loops until the size gets bigger,
    // return the new size.
    fn poll(&self) -> Result<Range<u64>> {
        loop {
            let cursor = match self.cursor {
                Some(c) => c,
                None => {
                    let md = fs::metadata(&self.path).chain_err(|| "unable to get metadata")?;
                    if md.len() == 0 { 0 } else { md.len() - 1 }
                }
            };

            let len = {
                fs::metadata(&self.path).chain_err(|| "unable to get metadata")?.len()
            };

            if len > cursor {
                trace!("{}: new stuff: cursor {} len {}",
                       self.path_as_lossy_str(),
                       cursor,
                       len);
                return Ok((cursor..len));
            }
            if cursor > len {
                trace!("{}: truncate: cursor {} len {}",
                       self.path_as_lossy_str(),
                       cursor,
                       len);
                // file truncated. Try to start again from beginning
                // We could also try to communicate this to the user from here.
                return Ok((0..len));
            } else {
                trace!("{}: nothing new, sleeping. cursor: {} len {}",
                       self.path_as_lossy_str(),
                       cursor,
                       len);
                thread::sleep(Duration::from_secs(1));
            }
        }
    }

    fn path_as_lossy_str(&self) -> Cow<str> {
        self.path.as_os_str().to_string_lossy()
    }
}

#[cfg(test)]
mod tests {
    extern crate tempdir;
    extern crate timebomb;

    use setup_logging;
    use msg::Msg;
    use self::tempdir::TempDir;
    use self::timebomb::timeout_ms;
    use std::fs::File;
    use std::fs::OpenOptions;
    use std::io::prelude::*;
    use std::path::PathBuf;
    use std::sync::atomic::ATOMIC_BOOL_INIT;
    use std::sync::atomic::AtomicBool;
    use std::sync::atomic::Ordering;
    use std::sync::mpsc::Receiver;
    use std::sync::mpsc::Sender;
    use std::sync::mpsc::channel;
    use std::thread;
    use super::*;
    use super::BUFFER_SIZE;

    static LOGGER_IS_SETUP: AtomicBool = ATOMIC_BOOL_INIT;

    fn new_worker(path: PathBuf,
                  cursor: Option<u64>)
                  -> (Worker, Sender<Option<Buffer>>, Receiver<Msg>) {
        let (test_tx, test_rx) = channel();
        let (self_tx, self_rx) = channel();

        let worker = Worker::new_with_cursor(path.clone(), test_tx, self_rx, cursor).unwrap();

        (worker, self_tx, test_rx)
    }

    fn with_setup<F>(f: F)
        where F: 'static + Fn(TempDir) -> () + Send
    {
        if !LOGGER_IS_SETUP.compare_and_swap(false, true, Ordering::SeqCst) {
            setup_logging("log/test.log").expect("log");
        }

        let tmpdir = TempDir::new("follow_worker_test").unwrap();

        timeout_ms(move || { f(tmpdir); }, 10000);
    }

    #[test]
    fn it_reads_lines_from_file_with_contents() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");
            let mut file = File::create(&path).unwrap();
            file.write(b"hi there").unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            match rx.recv().unwrap() {
                Msg::Read(buf) => {
                    assert_eq!(buf.get_bytes().len(), 8);
                    assert_eq!(String::from_utf8_lossy(buf.get_bytes()), "hi there");
                }
                msg => panic!("unexpected msg: {:?}", msg),
            };
        });
    }

    #[test]
    fn it_retuns_none_if_file_doesnt_exist_before_worker_is_created() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");
            let (mut worker, _, rx) = new_worker(path.clone(), None);

            assert!(worker.run().is_err());

            match rx.recv().unwrap() {
                Msg::Exit(_) => {}
                msg => panic!("unexpected msg: {:?}", msg),
            }
        });
    }

    #[test]
    fn it_retuns_none_if_file_is_deleted_after_worker_is_created_but_before_it_runs() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");
            let file = File::create(&path).unwrap();
            file.sync_all().unwrap();

            let (mut worker, _, rx) = new_worker(path.clone(), None);

            tmpdir.close().unwrap();

            match worker.run() {
                Ok(_) => {}
                Err(_) => {}
            };

            match rx.recv().unwrap() {
                Msg::Exit(_) => {}
                msg => panic!("unexpected msg: {:?}", msg),
            }
        });
    }

    #[test]
    fn it_retuns_none_if_file_is_deleted_after_worker_runs() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            file.write(b"hi there").unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            let handle = thread::spawn(move || {
                assert!(worker.run().is_ok());
            });

            // first buffer has text
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            match rx.recv().unwrap() {
                Msg::Read(buf) => {
                    assert_eq!(buf.get_bytes().len(), 8);
                    assert_eq!(String::from_utf8_lossy(buf.get_bytes()), "hi there");
                }
                msg => panic!("unexpected msg: {:?}", msg),
            };

            // second buffer is empty
            tmpdir.close().unwrap();
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            match rx.recv().unwrap() {
                Msg::Exit(_) => {}
                msg => panic!("unexpected msg: {:?}", msg),
            }

            handle.join().unwrap();
        });
    }

    // Can't seem to make file unreadable in stable
    //#[test]
    //fn it_retuns_none_if_file_cant_be_read() {
    ////with_setup(|tmpdir| {
    ////let path = tmpdir.path().join("test.txt");

    ////let mut file = File::create(&path).unwrap();
    ////let metadata = file.metadata()?;
    ////fs::set_permissions(&path, metadata.permissions());
    ////file.sync_all().unwrap();

    ////let (mut worker, _, rx) = new_worker(path.clone(), None);

    ////assert!(worker.run().is_err());

    ////let option = rx.recv().unwrap();
    ////assert!(option.is_none());
    ////});
    //}

    #[test]
    fn it_continues_reading_after_truncation() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            file.write(b"hi there").unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            let handle = thread::spawn(move || {
                assert!(worker.run().is_ok());
            });

            // first buffer should have text
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            match rx.recv().unwrap() {
                Msg::Read(buf) => {
                    assert_eq!(buf.get_bytes().len(), 8);
                    assert_eq!(String::from_utf8_lossy(buf.get_bytes()), "hi there");
                }
                msg => panic!("unexpected msg: {:?}", msg),
            };

            // truncate file and write to it
            drop(file);
            let mut file = OpenOptions::new().write(true).truncate(true).open(&path).unwrap();
            file.write(b"boo").unwrap();
            file.sync_all().unwrap();

            // second buffer should have text too
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            match rx.recv().unwrap() {
                Msg::Read(buf) => {
                    assert_eq!(buf.get_bytes().len(), 3);
                    assert_eq!(String::from_utf8_lossy(buf.get_bytes()), "boo");
                }
                msg => panic!("unexpected msg: {:?}", msg),
            };

            tx.send(None).unwrap();
            handle.join().unwrap();
        });
    }

    static ASCII_LOWER: [char; 26] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                                      'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x',
                                      'y', 'z'];

    fn build_sized_string(size: usize) -> String {
        (0..size).map(|i| ASCII_LOWER[i % ASCII_LOWER.len()]).collect()
    }

    #[test]
    fn it_reads_a_byte_of_text() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            let msg = build_sized_string(1);
            file.write(msg.as_bytes()).unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            match rx.recv().unwrap() {
                Msg::Read(buf) => {
                    assert_eq!(buf.get_bytes(), msg.as_bytes());
                }
                msg => panic!("unexpected msg: {:?}", msg),
            };
        });
    }

    #[test]
    fn it_reads_new_text_less_than_buffer_size() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            let msg = build_sized_string(BUFFER_SIZE / 2);
            file.write(msg.as_bytes()).unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            match rx.recv().unwrap() {
                Msg::Read(buf) => {
                    assert_eq!(buf.get_bytes(), msg.as_bytes());
                }
                msg => panic!("unexpected msg: {:?}", msg),
            };
        });
    }

    #[test]
    fn it_reads_new_text_one_byte_less_than_buffer_size() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            let msg = build_sized_string(BUFFER_SIZE - 1);
            file.write(msg.as_bytes()).unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            match rx.recv().unwrap() {
                Msg::Read(buf) => {
                    assert_eq!(buf.get_bytes(), msg.as_bytes());
                }
                msg => panic!("unexpected msg: {:?}", msg),
            };
        });
    }


    #[test]
    fn it_reads_new_text_equal_to_buffer_size() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            let msg = build_sized_string(BUFFER_SIZE);
            file.write(msg.as_bytes()).unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            match rx.recv().unwrap() {
                Msg::Read(buf) => assert_eq!(buf.get_bytes(), msg.as_bytes()),
                msg => panic!("unexpected msg: {:?}", msg),
            };
        });
    }

    #[test]
    fn it_reads_new_text_one_byte_larger_than_buffer_size() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            let content = build_sized_string(BUFFER_SIZE as usize + 1);
            file.write(content.as_bytes()).unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            let mut output = vec![];

            for _ in 0..2 {
                match rx.recv().unwrap() {
                    Msg::Read(buf) => {
                        output.extend_from_slice(buf.get_bytes());
                    }
                    msg => panic!("unexpected msg: {:?}", msg),
                };
            }

            assert_eq!(output.as_slice(), content.as_bytes());
        });
    }

    #[test]
    fn it_reads_new_text_between_one_and_two_buffers_in_size() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            let content = build_sized_string((BUFFER_SIZE as f32 * 1.5) as usize);
            file.write(content.as_bytes()).unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            let mut output = vec![];

            for _ in 0..2 {
                match rx.recv().unwrap() {
                    Msg::Read(buf) => {
                        output.extend_from_slice(buf.get_bytes());
                    }
                    msg => panic!("unexpected msg: {:?}", msg),
                };
            }

            assert_eq!(output.as_slice(), content.as_bytes());
        });
    }

    #[test]
    fn it_reads_new_text_one_byte_less_than_two_buffers_in_size() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            let content = build_sized_string((((BUFFER_SIZE as usize) * 2) - 1));
            file.write(content.as_bytes()).unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            let mut output = vec![];

            for _ in 0..2 {
                match rx.recv().unwrap() {
                    Msg::Read(buf) => {
                        output.extend_from_slice(buf.get_bytes());
                    }
                    msg => panic!("unexpected msg: {:?}", msg),
                };
            }

            assert_eq!(output.as_slice(), content.as_bytes());
        });
    }

    #[test]
    fn it_reads_new_text_two_buffers_in_size() {
        with_setup(|tmpdir| {
            let path = tmpdir.path().join("test.txt");

            let mut file = File::create(&path).unwrap();
            let content = build_sized_string(BUFFER_SIZE * 2);
            file.write(content.as_bytes()).unwrap();
            file.sync_all().unwrap();

            let (mut worker, tx, rx) = new_worker(path.clone(), Some(0));
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(Some(Buffer {
                    bytes: [0; BUFFER_SIZE],
                    path: path.clone(),
                    size: 0,
                }))
                .unwrap();
            tx.send(None).unwrap();

            worker.run().unwrap();

            let mut output = vec![];

            for _ in 0..2 {
                match rx.recv().unwrap() {
                    Msg::Read(buf) => {
                        output.extend_from_slice(buf.get_bytes());
                    }
                    msg => panic!("unexpected msg: {:?}", msg),
                };
            }

            assert_eq!(output.as_slice(), content.as_bytes());
        });
    }
}
